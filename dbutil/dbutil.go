package dbutil

import (
	"database/sql"
	"fmt"
	"log"

	bolt "github.com/coreos/bbolt"
)

// ConnectSQL Realiza la conexión con PostgresSQL.
func ConnectSQL(dbname string) *sql.DB {
	cnnString := fmt.Sprintf("host=localhost port=5432 user=postgres dbname=%s sslmode=disable", dbname)
	db, err := sql.Open("postgres", cnnString)

	if err != nil {
		log.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	return db
}

// ConnectNoSQL Realiza la conexión con BoltDB.
func ConnectNoSQL() *bolt.DB {
	db, err := bolt.Open("db1-tp.db", 0600, nil)

	if err != nil {
		log.Fatal(err)
	}

	return db
}
