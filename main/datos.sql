drop database if exists datos;
create database datos;

\c datos

create table cliente(
    nrocliente int,
    nombre text,
    apellido text,
    domicilio text,
    telefono char(12)
);

alter table cliente add constraint cliente_pk primary key (nrocliente);

create table tarjeta(
    nrotarjeta char(16),
    nrocliente int,
    validadesde char(6), --e.g. 201106
    validahasta char(6),
    codseguridad char(4),
    limitecompra decimal(8,2),
    estado char(10) --`vigente', `suspendida', `anulada'
);

alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente(nrocliente);

create table comercio(
    nrocomercio int,
    nombre text,
    domicilio text,
    codigopostal char(8),
    telefono char(12)
);


alter table comercio add constraint comercio_pk primary key (nrocomercio);


create table compra(
    nrooperacion int,
    nrotarjeta char(16),
    nrocomercio int,
    fecha timestamp,
    monto decimal(7,2),
    pagado boolean
);


alter table compra add constraint compra_pk primary key (nrooperacion);
alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table compra add constraint compra_comercio_fk foreign key (nrocomercio) references comercio(nrocomercio);


create table rechazo(
    nrorechazo int,
    nrotarjeta char(16),
    nrocomercio int,
    fecha timestamp,
    monto decimal(7,2),
    motivo text
);

alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
alter table rechazo add constraint rechazo_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table rechazo add constraint rechazo_comercio_fk foreign key (nrocomercio) references comercio(nrocomercio);



create table cierre(
    año int,
    mes int,
    terminacion int,
    fechainicio date,
    fechacierre date,
    fechavto date
);

alter table cierre add constraint cierre_pk primary key (año,mes,terminacion);


create table cabecera(
    nroresumen int,
    nombre text,
    apellido text,
    domicilio text,
    nrotarjeta char(16),
    desde date,
    hasta date,
    vence date,
    total decimal(8,2)
);
alter table cabecera add constraint cabecera_pk primary key (nroresumen);
alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);


create table detalle(
    nroresumen int,
    nrolinea int,
    fecha date,
    nombrecomercio text,
    monto decimal(7,2)
);
alter table detalle add constraint detalle_pk primary key (nroresumen,nrolinea);

create table alerta(
    nroalerta int,
    nrotarjeta char(16),
    fecha timestamp,
    nrorechazo int,
    codalerta int, --0:rechazo, 1:compra 1min, 5:compra 5min, 32:límite
    descripcion text
);
alter table alerta add constraint alerta_pk primary key (nroalerta);
alter table alerta add constraint alerta_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta);
alter table alerta add constraint alerta_nrorechazo_fk foreign key (nrorechazo) references rechazo(nrorechazo);


-- Esta tabla no es parte del modelo de datos, pero se incluye para
-- poder probar las funciones.
create table consumo(
    nrotarjeta char(16),
    codseguridad char(4),
    nrocomercio int,
    monto decimal(7,2)
);


create table prueba(
    id int,
    fecha date
);

insert into prueba values(1,' 2019-06-01');
insert into prueba values(2,' 2019-06-23');
insert into prueba values(3,' 2019-07-02');
insert into prueba values(4,' 2019-07-08');

select * from prueba where fecha between '2019-06-22'and '2019-07-02';


alter table tarjeta drop constraint tarjeta_nrocliente_fk;
alter table compra drop constraint compra_nrotarjeta_fk;
alter table compra drop constraint compra_comercio_fk;
alter table rechazo drop constraint rechazo_comercio_fk;
alter table rechazo drop constraint rechazo_nrotarjeta_fk;
alter table cabecera drop constraint cabecera_nrotarjeta_fk;
alter table alerta drop constraint alerta_nrorechazo_fk;
alter table alerta drop constraint alerta_nrotarjeta_fk;


alter table cliente drop constraint cliente_pk;
alter table tarjeta drop constraint tarjeta_pk;
alter table comercio drop constraint comercio_pk;
alter table compra drop constraint compra_pk;
alter table rechazo drop constraint rechazo_pk;
alter table cierre drop constraint cierre_pk;
alter table cabecera drop constraint cabecera_pk;
alter table detalle drop constraint detalle_pk;
alter table alerta drop constraint alerta_pk;


insert into tarjeta values ('aaaabbbbcccceee1', 1, '201008', '201908','abcd',10000,'vigente');
insert into tarjeta values ('aaaabbbbcccceee2', 1, '201008', '201908','abce',10000,'suspendida');
insert into tarjeta values ('aaaabbbbcccceee3', 1, '201008', '201908','abcf',10000,'anulada');
insert into tarjeta values ('aaaabbbbcccceee4', 1, '201812', '201902','abcd',10000,'vigente');
insert into tarjeta values ('aaaabbbbcccceee5', 1, '202008', '202108','abce',10000,'suspendida');
insert into tarjeta values ('aaaabbbbcccceee6', 1, '201001', '201101','abcf',10000,'anulada');
insert into tarjeta values ('aaaabbbbcccceee7', 1, '201901', '201912','abcd',10000,'suspendida');

insert into compra values(1,'aaaabbbbcccceee1',1,'2019-06-23',1000.60,false);
insert into compra values(1,'aaaabbbbcccceee1',1,'2019-06-23',1000.60,true);
insert into compra values(1,'aaaabbbbcccceee1',1,'2019-06-23',1200.20,false);
insert into compra values(1,'aaaabbbbcccceee1',1,'2019-06-23',10000,false);


--RETORNA VERDADERO SI LA TARJETA ES VALIDA
create or replace function validar_tarjeta_nro(p_nrotarjeta char(16)) returns boolean as $$
declare
    estado boolean=false;
begin
if exists (select nrotarjeta from tarjeta where nrotarjeta=p_nrotarjeta) then
     return true;
end if;	
	return false;
end;
$$ language plpgsql;

--RETORNA VERDADERO SI LA TARJETA SE ENCUENTRA VIGENTE
create or replace function validar_tarjeta_vigente(p_nrotarjeta char(16)) returns boolean as $$
declare
      var int default 0;
begin
	select count(1) into var from tarjeta where estado='vigente' and nrotarjeta=p_nrotarjeta;
IF var > 0 THEN
     return true;
end if;	
	return false;
end;
$$ language plpgsql;


--RETORNA VERDADERO SI EL CODIGO DE SEGURIDAD CORRESPONDE A LA TARJETA
create or replace function validar_tarjeta_codseg(p_nrotarjeta char(16), p_codseg char(4)) returns boolean as $$
declare
      var int default 0;
begin
	select count(1) into var from tarjeta where codseguridad=p_codseg and nrotarjeta=p_nrotarjeta;
IF var > 0 THEN
     return true;
end if;	
	return false;
end;
$$ language plpgsql;

--RETORNA VERDADERO SI LA TARJETA NO SE ENCUENTRA SUSPENDIDA

create or replace function validar_tarjeta_suspendida(p_nrotarjeta char(16)) returns boolean as $$
declare
      var int default 0;
begin
	select count(1) into var from tarjeta where estado='suspendida' and nrotarjeta=p_nrotarjeta;
IF var > 0 THEN
     return false;
end if;	
	return true;
end;
$$ language plpgsql;




--RETORNA VERDADERO SI LA TARJETA SE ENCUENTRA ACTIVA
create or replace function validar_tarjeta_vencida(p_nrotarjeta char(16), fecha date) returns boolean as $$
declare
	var int default 0;
	resultado record;
	var_desde char(12);
	var_fecha_desde date;	
	var_hasta char(12);
	var_fecha_hasta date;
begin
	select * into resultado from tarjeta where nrotarjeta=p_nrotarjeta;
	if not found then
       		 raise notice 'la tarjeta % es inválida', p_nrotarjeta;
	else
		var_desde := (substring(resultado.validadesde from 0 for 5))||(substring(resultado.validadesde from 5 for 2))||'01'; 
		var_fecha_desde :=to_date(var_desde,'YYYYMMDD');
		var_hasta := (substring(resultado.validahasta from 0 for 5))|| '-'||(substring(resultado.validahasta from 5 for 2))||'-01'; 
		var_fecha_hasta :=  (date_trunc('month', var_hasta ::date) + interval '1 month' - interval '1 day')::date;
		select count(1) into var from tarjeta where nrotarjeta=p_nrotarjeta and fecha between var_fecha_desde and   var_fecha_hasta;
		IF var > 0 THEN
			return true;
		else
			return false;
		end if;	
	end if;
     return true;
end;
$$ language plpgsql;

--RETORNA VERDADERO SI SE PUEDE REALIZAR LA COMPRA

create or replace function validar_tarjeta_compras_pendientes(p_nrotarjeta char(16), monto_compra_actual decimal(8,2)) returns boolean as $$
declare
	varLimite decimal(8,2) default 0;
	varMontoGastado decimal(8,2) default 0;
	resultado record;
    comprarecord record;
begin
	select limitecompra into varLimite from tarjeta where nrotarjeta=p_nrotarjeta;
	select * into comprarecord from compra where nrotarjeta=p_nrotarjeta and pagado='false';
	if not found then
	    varMontoGastado := 0;
    else
        select SUM(monto) into varMontoGastado from compra where nrotarjeta=p_nrotarjeta and pagado='false';
    end if;
	if((varMontoGastado+monto_compra_actual)<= varLimite)then
		return true;
	else
		return false;
	end if;
end;
$$ language plpgsql;


create or replace function insertar_rechazo(p_nrotarjeta char(16), p_nrocomercio int, p_fecha timestamp, p_monto decimal(7,2), p_motivo text) returns void as $$
declare
		 var int;
		 rechazado record;
begin
		select * into rechazado from rechazo where nrorechazo=1;
		if not found then
			var := 1;
		else
			select max(nrorechazo) into var from rechazo;
			var := var+1;	
		end if;
			insert into rechazo values (var, p_nrotarjeta,  p_nrocomercio,p_fecha, p_monto,p_motivo);
end;
$$ language plpgsql;


create or replace function agregar_compra(p_nrotarjeta char(16),p_nrocomercio int,p_fecha timestamp,p_monto decimal(7,2),p_pagado boolean) returns void as $$
declare
		 var int;
		 comprar record;
begin
		select * into comprar from compra where nrooperacion=1;
		if not found then
			var := 1;
		else
			select max(nrooperacion) into var from compra;
			var := var+1;	
		end if;
			insert into compra values(var,p_nrotarjeta,p_nrocomercio,p_fecha,p_monto,p_pagado);
end;
$$ language plpgsql;


create or replace function insertarcompra(p_nrotarjeta char(16), codseguridad char(4),nrocomercio int, monto decimal(7,2)) returns boolean as $$
declare
    estado boolean;
    motivo text;
begin
	estado := true;
	if(select validar_tarjeta_nro(p_nrotarjeta)) then
		raise notice 'la tarjeta es válida';

        if(select validar_tarjeta_vigente(p_nrotarjeta)) THEN
            raise notice 'la tarjeta es vigente';

            if(select validar_tarjeta_codseg(p_nrotarjeta,codseguridad)) THEN
                raise notice 'El codigo de seguridad es correcto';

                if(select validar_tarjeta_compras_pendientes(p_nrotarjeta,monto)) THEN
                    raise notice 'No supera el limite de la tarjeta';

                    if(select validar_tarjeta_vencida(p_nrotarjeta,CURRENT_DATE)) THEN
                        raise notice 'La tarjeta no se encuentra vencida';

                ELSE
                    raise notice 'Plazo de vigencia expirado';
                    estado := false;
                    motivo := 'Plazo de vigencia expirado';
                end if;  

                ELSE
                    raise notice 'Supera el lìmite de compra';
                    estado := false;
                    motivo := 'Supera el lìmite de compra';
                end if;  

            ELSE
                raise notice 'El codigo de seguridad no es correcto';
                estado := false;
                motivo := 'El codigo de seguridad no es correcto';
            end if;  

        ELSE
            raise notice 'la tarjeta no es vigente';
            estado := false;
            motivo := 'La tarjeta no es vigente';

            --AGREGO CODIGO DE TARJETA SUSPENDIDA

            if(select validar_tarjeta_suspendida(p_nrotarjeta)) THEN
                raise notice 'La tarjeta no se encuentra suspendida';
                ELSE
                raise notice 'La tarjeta se encuentra suspendida';
                PERFORM insertar_rechazo(p_nrotarjeta,nrocomercio::int, CURRENT_TIMESTAMP::timestamp,monto, 'La tarjeta se encuentra suspendida');
            end if;  

         end if;    
        
	else
		raise notice 'la tarjeta es inválida se guarda rechazo';
        motivo := 'La tarjeta no es invalida';
        estado := false;
        end if;

    if(estado = false)then
        PERFORM insertar_rechazo(p_nrotarjeta,nrocomercio::int, CURRENT_TIMESTAMP::timestamp,monto, motivo);
    else
        PERFORM agregar_compra(p_nrotarjeta,nrocomercio,CURRENT_TIMESTAMP::timestamp,monto,false);
    end if;

	return true;
end;
$$ language plpgsql;
