package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strconv"

	"../dbutil"
	"../promptutil"
	bolt "github.com/coreos/bbolt"
	_ "github.com/lib/pq"
)

// #region Types

// Cliente modelo de cliente
type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

// Tarjeta modelo de tarjeta
type Tarjeta struct {
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad string
	LimiteCompra float64
	Estado       string
}

// Comercio modelo de comercio
type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal string
	Telefono     string
}

// Compra modelo de compra
type Compra struct {
	NroOperacion int
	NroTarjeta   string
	NroComercio  int
	Fecha        string
	Monto        float64
	Pagado       bool
}

//endregion

// #region Declarations
var sqldb *sql.DB
var boltdb *bolt.DB

var customers []Cliente
var cards []Tarjeta
var stores []Comercio
var purchases []Compra

//endregion

func main() {
	mainMenu()
}

// #region Menus
func mainMenu() {
	choice := promptutil.Menu([]string{
		"Ejercicios SQL",
		"Ejercicios NoSQL",
		"Salir",
	})

	switch choice {
	case 1:
		sqlMenu()
	case 2:
		noSQLMenu()
	case 3:
		if sqldb != nil {
			sqldb.Close()
		}
		if boltdb != nil {
			boltdb.Close()
		}
		os.Exit(0)
	}
}

func sqlMenu() {
	if sqldb == nil {
		databaseConnection()
	}

	choice := promptutil.Menu([]string{
		"Crear tablas",
		"Crear PK's y FK's",
		"Quitar PK's y FK's",
		"Llenar tablas",
		"Crear triggers y store procedures",
		"Probar sistema",
		"Menú anterior",
	})

	switch choice {
	case 1:
		createTables()
		fmt.Println("Tablas creadas")
		sqlMenu()
	case 2:
		createPKTables()
		fmt.Println("Pk's creadas")
		createFKTables()
		fmt.Println("Fk's creadas")
		sqlMenu()
	case 3:
		removeFKTables()
		fmt.Println("Pk's removidas")
		removePKTables()
		fmt.Println("Fk's removidas")
		sqlMenu()
	case 4:
		fillTables()
		insertConsumo()
		fmt.Println("Tablas cargadas")
		sqlMenu()
	case 5:
		executePurchase()
		generateSummary()
		createTriggerFunctions()
		fmt.Println("Triggers y store procedures creados")
		sqlMenu()
	case 6:
		testSystem()
		sqlMenu()
	case 7:
		mainMenu()
	}
}

func noSQLMenu() {
	if boltdb == nil {
		boltdb = dbutil.ConnectNoSQL()
	}

	choice := promptutil.Menu([]string{
		"Crear instancias de datos",
		"Guardar en base de datos",
		"Mostrar información guardada",
		"Menú anterior",
	})

	switch choice {
	case 1:
		createInstances()
		fmt.Println("Instancias de datos creadas")
		noSQLMenu()
	case 2:
		writeCustomers()
		writeCards()
		writeStores()
		writePurchases()
		fmt.Println("Se guardó en la base de datos")
		noSQLMenu()
	case 3:
		readNoSQLMenu()
	case 4:
		mainMenu()
	}
}

func readNoSQLMenu() {
	choice := promptutil.Menu([]string{
		"Mostrar Clientes",
		"Mostrar Tarjetas",
		"Mostrar Comercios",
		"Mostrar Compras",
		"Menú anterior",
	})

	switch choice {
	case 1:
		for _, customer := range customers {
			read("clientes", []byte(strconv.Itoa(customer.NroCliente)))
		}
		readNoSQLMenu()
	case 2:
		for _, card := range cards {
			read("tarjetas", []byte(card.NroTarjeta))
		}
		readNoSQLMenu()
	case 3:
		for _, store := range stores {
			read("comercios", []byte(strconv.Itoa(store.NroComercio)))
		}
		readNoSQLMenu()
	case 4:
		for _, purchase := range purchases {
			read("compras", []byte(strconv.Itoa(purchase.NroOperacion)))
		}
		readNoSQLMenu()
	case 5:
		noSQLMenu()
	}
}

// endregion

// #region SQL Operations
func databaseConnection() {
	var exists bool
	db := dbutil.ConnectSQL("postgres")
	row := db.QueryRow("SELECT true FROM pg_database WHERE datname = 'datos';")

	switch err := row.Scan(&exists); err {
	case sql.ErrNoRows:
		fmt.Println("Creating database")
		db.Exec("CREATE DATABASE datos;")
		db.Close()

		fmt.Println("Connecting to database")
		sqldb = dbutil.ConnectSQL("datos")
	case nil:
		fmt.Println("Connecting to database")
		sqldb = dbutil.ConnectSQL("datos")
	default:
		log.Fatal(err)
	}
}

func createTables() {
	statement := `
		CREATE TABLE IF NOT EXISTS cliente(
			nrocliente INT,
			nombre TEXT,
			apellido TEXT,
			domicilio TEXT,
			telefono CHAR(12)
		);

		CREATE TABLE IF NOT EXISTS tarjeta(
			nrotarjeta CHAR(16),
			nrocliente INT,
			validadesde CHAR(6),
			validahasta CHAR(6),
			codseguridad CHAR(4),
			limitecompra DECIMAL(8,2),
			estado CHAR(10)
		);

		CREATE TABLE IF NOT EXISTS comercio(
			nrocomercio INT,
			nombre TEXT,
			domicilio TEXT,
			codigopostal CHAR(8),
			telefono CHAR(12)
		);

		CREATE TABLE IF NOT EXISTS compra(
			nrooperacion INT,
			nrotarjeta CHAR(16),
			nrocomercio INT,
			fecha TIMESTAMP,
			monto DECIMAL(7,2),
			pagado BOOLEAN
		);

		CREATE TABLE IF NOT EXISTS rechazo(
			nrorechazo INT,
			nrotarjeta CHAR(16),
			nrocomercio INT,
			fecha TIMESTAMP,
			monto DECIMAL(7,2),
			motivo TEXT
		);

		CREATE TABLE IF NOT EXISTS cierre(
			año INT,
			mes INT,
			terminacion INT,
			fechainicio DATE,
			fechacierre DATE,
			fechavto DATE
		);

		CREATE TABLE IF NOT EXISTS cabecera(
			nroresumen INT,
			nombre TEXT,
			apellido TEXT,
			domicilio TEXT,
			nrotarjeta CHAR(16),
			desde DATE,
			hasta DATE,
			vence DATE,
			total DECIMAL(8,2)
		);

		CREATE TABLE IF NOT EXISTS detalle(
			nroresumen INT,
			nrolinea INT,
			fecha DATE,
			nombrecomercio TEXT,
			monto DECIMAL(7,2)
		);

		CREATE TABLE IF NOT EXISTS alerta(
			nroalerta INT,
			nrotarjeta CHAR(16),
			fecha TIMESTAMP,
			nrorechazo INT,
			codalerta INT,
			descripcion TEXT
		);

		CREATE TABLE IF NOT EXISTS consumo(
			nrotarjeta CHAR(16),
			codseguridad CHAR(4),
			nrocomercio INT,
			monto DECIMAL(7,2)
		);`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func createPKTables() {
	db := sqldb

	removeFKTables()
	removePKTables()

	_, err := db.Exec(`
		ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY (nrocliente);
		ALTER TABLE tarjeta ADD CONSTRAINT tarjeta_pk PRIMARY KEY (nrotarjeta);
		ALTER TABLE comercio ADD CONSTRAINT comercio_pk PRIMARY KEY (nrocomercio);
		ALTER TABLE compra ADD CONSTRAINT compra_pk PRIMARY KEY (nrooperacion);
		ALTER TABLE rechazo ADD CONSTRAINT rechazo_pk PRIMARY KEY (nrorechazo);
		ALTER TABLE cierre ADD CONSTRAINT cierre_pk PRIMARY KEY (año,mes,terminacion);
		ALTER TABLE cabecera ADD CONSTRAINT cabecera_pk PRIMARY KEY (nroresumen);
		ALTER TABLE detalle ADD CONSTRAINT detalle_pk PRIMARY KEY (nroresumen,nrolinea);
		ALTER TABLE alerta ADD CONSTRAINT alerta_pk PRIMARY KEY (nroalerta);
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func createFKTables() {
	db := sqldb

	_, err := db.Exec(`
		ALTER TABLE tarjeta ADD CONSTRAINT tarjeta_nrocliente_fk FOREIGN KEY (nrocliente) REFERENCES cliente(nrocliente);
		ALTER TABLE compra ADD CONSTRAINT compra_nrotarjeta_fk FOREIGN KEY (nrotarjeta) REFERENCES tarjeta(nrotarjeta);
		ALTER TABLE compra ADD CONSTRAINT compra_comercio_fk FOREIGN KEY (nrocomercio) REFERENCES comercio(nrocomercio);
		ALTER TABLE rechazo ADD CONSTRAINT rechazo_comercio_fk FOREIGN KEY (nrocomercio) REFERENCES comercio(nrocomercio);
		ALTER TABLE cabecera ADD CONSTRAINT cabecera_nrotarjeta_fk FOREIGN KEY (nrotarjeta) REFERENCES tarjeta(nrotarjeta);
		ALTER TABLE alerta ADD CONSTRAINT alerta_nrorechazo_fk FOREIGN KEY (nrorechazo) REFERENCES rechazo(nrorechazo);
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func removePKTables() {
	db := sqldb

	_, err := db.Exec(`
		ALTER TABLE cliente DROP CONSTRAINT IF EXISTS cliente_pk;
		ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_pk;
		ALTER TABLE comercio DROP CONSTRAINT IF EXISTS comercio_pk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS compra_pk;
		ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS rechazo_pk;
		ALTER TABLE cierre DROP CONSTRAINT  IF EXISTS cierre_pk;
		ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS cabecera_pk;
		ALTER TABLE detalle DROP CONSTRAINT IF EXISTS detalle_pk;
		ALTER TABLE alerta DROP CONSTRAINT IF EXISTS alerta_pk;
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func removeFKTables() {
	db := sqldb

	_, err := db.Exec(`
		ALTER TABLE tarjeta DROP CONSTRAINT IF EXISTS tarjeta_nrocliente_fk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS  compra_nrotarjeta_fk;
		ALTER TABLE compra DROP CONSTRAINT IF EXISTS  compra_comercio_fk;
		ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS  rechazo_comercio_fk;
		ALTER TABLE rechazo DROP CONSTRAINT IF EXISTS  rechazo_nrotarjeta_fk;
		ALTER TABLE cabecera DROP CONSTRAINT IF EXISTS  cabecera_nrotarjeta_fk;
		ALTER TABLE alerta DROP CONSTRAINT IF EXISTS  alerta_nrorechazo_fk;
		ALTER TABLE alerta DROP CONSTRAINT IF EXISTS  alerta_nrotarjeta_fk;
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func fillTables() {
	db := sqldb

	_, err := db.Exec(`
		INSERT INTO cliente VALUES(1, 'MICHAEL', 'GREEN', '1906 Green Acres Road', '252-434-6868');
		INSERT INTO cliente VALUES(2, 'SOPHIA', 'DAVIS', '3900 Rocket Drive', '612-961-9722');
		INSERT INTO cliente VALUES(3, 'ABIGAIL', 'PHILLIPS', '144 Cambridge Court', '479-751-4614');
		INSERT INTO cliente VALUES(4, 'MATTHEW', 'MOORE', '4215 Walnut Hill Drive', '513-222-9055');
		INSERT INTO cliente VALUES(5, 'GABRIEL', 'ADAMS', '4722 Coal Street', '814-280-0574');
		INSERT INTO cliente VALUES(6, 'GRACE', 'JOHNSON', '3762 Walnut Street', '601-641-4193');
		INSERT INTO cliente VALUES(7, 'CHRISTOPHER', 'COLLINS', '349 Benson Street', '715-322-0783');
		INSERT INTO cliente VALUES(8, 'HUNTER', 'DAVIS', '2933 Kildeer Drive', '757-215-8549');
		INSERT INTO cliente VALUES(9, 'DAVID', 'EVANS', '3965 Rollins Road', '308-684-6587');
		INSERT INTO cliente VALUES(10, 'ANTHONY', 'PEREZ', '1154 Liberty Avenue', '714-485-0722');
		INSERT INTO cliente VALUES(11, 'ALYSSA', 'CARTER', '4273 Godfrey Street', '503-575-2088');
		INSERT INTO cliente VALUES(12, 'JOSHUA', 'ALLEN', '3648 Barrington Court', '870-587-6709');
		INSERT INTO cliente VALUES(13, 'ASHLEY', 'LOPEZ', '36 Rose Street', '708-207-1456');
		INSERT INTO cliente VALUES(14, 'ISABELLA', 'ROBERTS', '2823 Chenoweth Drive', '931-516-3516');
		INSERT INTO cliente VALUES(15, 'JAMES', 'TAYLOR', '1825 White Oak Drive', '816-689-0220');
		INSERT INTO cliente VALUES(16, 'JOSEPH', 'GREEN', '4138 Chestnut Street', '727-470-1046');
		INSERT INTO cliente VALUES(17, 'GRACE', 'GARCIA', '2193 Losh Lane', '412-771-7293');
		INSERT INTO cliente VALUES(18, 'JAMES', 'BAKER', '2932 Gateway Road', '503-277-9629');
		INSERT INTO cliente VALUES(19, 'OLIVIA', 'EVANS', '3523 Grant Street', '903-892-4046');
		INSERT INTO cliente VALUES(20, 'JOSHUA', 'WILLIAMS', '2244 Woodbridge Lane', '517-702-6355');


		INSERT INTO tarjeta VALUES('3489558385502970', 1, '201106', '202308', '5060', 5000.00, 'vigente');
		INSERT INTO tarjeta VALUES('5457557367903750', 2, '201106', '202401', '4280', 250000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('4559588866746051', 3, '201106', '202101', '8100', 180000.00, 'anulada');
		INSERT INTO tarjeta VALUES('5442334993275369', 4, '201106', '202205', '8710', 270000.00, 'vigente');
		INSERT INTO tarjeta VALUES('4930153821609231', 6, '201106', '202310', '6190', 120000.00, 'anulada');
		INSERT INTO tarjeta VALUES('5102620553138109', 7, '201106', '201702', '3550', 170000.00, 'vigente');	
		INSERT INTO tarjeta VALUES('5498756991338588', 8, '201106', '202208', '8700', 540000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('5529630673357120', 9, '201106', '202401', '1040', 100000.00, 'anulada');
		INSERT INTO tarjeta VALUES('3408631038282240', 10, '201106', '201907', '7020', 260000.00, 'vigente');
		INSERT INTO tarjeta VALUES('5229515299210253', 11, '201106', '202003', '3250', 230000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('4754982137770169', 12, '201106', '202010', '3300', 170000.00, 'anulada');
		INSERT INTO tarjeta VALUES('4664383242476058', 13, '201106', '202007', '7030', 120000.00, 'vigente');
		INSERT INTO tarjeta VALUES('4833159207333898', 14, '201106', '202304', '4910', 110000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('5221007196689423', 15, '201106', '202206', '5680', 250000.00, 'anulada');
		INSERT INTO tarjeta VALUES('3748427341747440', 16, '201106', '202303', '9350', 210000.00, 'vigente');
		INSERT INTO tarjeta VALUES('3425002185317010', 17, '201106', '202212', '3450', 220000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('3755947513532010', 18, '201106', '202301', '4170', 130000.00, 'anulada');
		INSERT INTO tarjeta VALUES('5435809384721294', 19, '201106', '202203', '2760', 360000.00, 'vigente');
		INSERT INTO tarjeta VALUES('3474667951177900', 20, '201106', '202104', '4030', 850000.00, 'suspendida');
		INSERT INTO tarjeta VALUES('4619521540625191', 5, '201106', '202107', '9870', 260000.00, 'vigente');
		INSERT INTO tarjeta VALUES('343915672231656', 5, '201106', '202309', '5570', 140000.00, 'vigente');
		INSERT INTO tarjeta VALUES('3897588866746057', 13, '201106', '202101', '8100', 700000.00, 'vigente');

		INSERT INTO comercio VALUES(1, 'Churrasic Park', '12230 Espadilla', '562', '685 394 144');
		INSERT INTO comercio VALUES(2, 'Delaostia', '44100 Albarracín', '437', '641 564 536');
		INSERT INTO comercio VALUES(3, 'Bar Der Troya', '08910 Badalona', '054', '626 260 336');
		INSERT INTO comercio VALUES(4, 'La Banana Generosa', '13630 Socuéllamos', '692', '755 440 257');
		INSERT INTO comercio VALUES(5, 'Breaking Bar', '37339 Villoruela', '266', '657 089 978');
		INSERT INTO comercio VALUES(6, 'Padel Nuestro', '15391 Cesuras', '261', '687 055 730');
		INSERT INTO comercio VALUES(7, 'Las Nuevas Novedades', '31579 Cárcar', '241', '679 099 044');
		INSERT INTO comercio VALUES(8, 'La Tienda Sin Nombre', '11200 Algeciras', '967', '758 681 025');
		INSERT INTO comercio VALUES(9, 'El Triangulo de Las Verduras', '26211 Fonzaleche', '212', '737 508 594');
		INSERT INTO comercio VALUES(10, 'BellonSe', '06460 Campanario', '299', '743 736 621');
		INSERT INTO comercio VALUES(11, 'FaceLook', '36830 A Lama', '724', '772 681 570');
		INSERT INTO comercio VALUES(12, 'Indiana Jeans', '41805 Benacazón', '039', '723 897 767');
		INSERT INTO comercio VALUES(13, 'Larry Plotter', '37214 Cabeza del Caballo', '556', '715 579 826');
		INSERT INTO comercio VALUES(14, 'Bread Pitt', '43850 Cambrils', '583', '750 538 093');
		INSERT INTO comercio VALUES(15, 'Florist Gump', '36994 Poio', '054', '678 524 261');
		INSERT INTO comercio VALUES(16, 'Carlos Duty', '47800 Medina de Rioseco', '503', '681 886 878');
		INSERT INTO comercio VALUES(17, 'Barber Streisand', '23500 Jódar', '343', '620 282 581');
		INSERT INTO comercio VALUES(18, 'El Fashion Donald', '04650 Zurgena', '244', '715 505 367');
		INSERT INTO comercio VALUES(19, 'Postres EQL', '02410 Liétor', '783', '798 261 534');
		INSERT INTO comercio VALUES(20, 'The Bird Canilla', '03650 Pinoso', '090', '788 908 407');

		INSERT INTO cierre VALUES(2019, 1,  0, '2018-12-05', '2019-01-01', '2019-01-20');
		INSERT INTO cierre VALUES(2019, 2,  0, '2019-01-02', '2019-01-29', '2019-02-17');
		INSERT INTO cierre VALUES(2019, 3,  0, '2019-01-30', '2019-02-26', '2019-03-17');
		INSERT INTO cierre VALUES(2019, 4,  0, '2019-02-27', '2019-03-26', '2019-04-14');
		INSERT INTO cierre VALUES(2019, 5,  0, '2019-03-27', '2019-04-23', '2019-05-12');
		INSERT INTO cierre VALUES(2019, 6,  0, '2019-04-24', '2019-05-21', '2019-06-09');
		INSERT INTO cierre VALUES(2019, 7,  0, '2019-05-22', '2019-06-18', '2019-07-07');
		INSERT INTO cierre VALUES(2019, 8,  0, '2019-06-19', '2019-07-16', '2019-08-04');
		INSERT INTO cierre VALUES(2019, 9,  0, '2019-07-17', '2019-08-13', '2019-09-01');
		INSERT INTO cierre VALUES(2019, 10, 0, '2019-08-14', '2019-09-10', '2019-09-29');
		INSERT INTO cierre VALUES(2019, 11, 0, '2019-09-11', '2019-10-08', '2019-10-27');
		INSERT INTO cierre VALUES(2019, 12, 0, '2019-10-09', '2020-11-05', '2020-11-24');

		INSERT INTO cierre VALUES(2019, 1,  1, '2018-12-06', '2019-01-02', '2019-01-21');
		INSERT INTO cierre VALUES(2019, 2,  1, '2019-01-03', '2019-01-30', '2019-02-18');
		INSERT INTO cierre VALUES(2019, 3,  1, '2019-01-31', '2019-02-27', '2019-03-18');
		INSERT INTO cierre VALUES(2019, 4,  1, '2019-02-28', '2019-03-27', '2019-04-15');
		INSERT INTO cierre VALUES(2019, 5,  1, '2019-03-28', '2019-04-24', '2019-05-13');
		INSERT INTO cierre VALUES(2019, 6,  1, '2019-04-25', '2019-05-22', '2019-06-10');
		INSERT INTO cierre VALUES(2019, 7,  1, '2019-05-23', '2019-06-19', '2019-07-08');
		INSERT INTO cierre VALUES(2019, 8,  1, '2019-06-20', '2019-07-17', '2019-08-05');
		INSERT INTO cierre VALUES(2019, 9,  1, '2019-07-18', '2019-08-14', '2019-09-02');
		INSERT INTO cierre VALUES(2019, 10, 1, '2019-08-15', '2019-09-11', '2019-09-30');
		INSERT INTO cierre VALUES(2019, 11, 1, '2019-09-12', '2019-10-09', '2019-10-28');
		INSERT INTO cierre VALUES(2019, 12, 1, '2019-10-10', '2020-11-06', '2020-11-25');

		INSERT INTO cierre VALUES(2019, 1,  2, '2018-12-07', '2019-01-03', '2019-01-22');
		INSERT INTO cierre VALUES(2019, 2,  2, '2019-01-04', '2019-01-31', '2019-02-19');
		INSERT INTO cierre VALUES(2019, 3,  2, '2019-02-01', '2019-02-28', '2019-03-19');
		INSERT INTO cierre VALUES(2019, 4,  2, '2019-03-01', '2019-03-28', '2019-04-16');
		INSERT INTO cierre VALUES(2019, 5,  2, '2019-03-29', '2019-04-25', '2019-05-14');
		INSERT INTO cierre VALUES(2019, 6,  2, '2019-04-26', '2019-05-23', '2019-06-11');
		INSERT INTO cierre VALUES(2019, 7,  2, '2019-05-24', '2019-06-20', '2019-07-09');
		INSERT INTO cierre VALUES(2019, 8,  2, '2019-06-21', '2019-07-18', '2019-08-06');
		INSERT INTO cierre VALUES(2019, 9,  2, '2019-07-19', '2019-08-15', '2019-09-03');
		INSERT INTO cierre VALUES(2019, 10, 2, '2019-08-16', '2019-09-12', '2019-10-01');
		INSERT INTO cierre VALUES(2019, 11, 2, '2019-09-13', '2019-10-10', '2019-10-29');
		INSERT INTO cierre VALUES(2019, 12, 2, '2019-10-11', '2020-11-07', '2020-11-26');

		INSERT INTO cierre VALUES(2019, 1,  3, '2018-12-08', '2019-01-04', '2019-01-23');
		INSERT INTO cierre VALUES(2019, 2,  3, '2019-01-05', '2019-02-01', '2019-02-20');
		INSERT INTO cierre VALUES(2019, 3,  3, '2019-02-02', '2019-03-01', '2019-03-20');
		INSERT INTO cierre VALUES(2019, 4,  3, '2019-03-02', '2019-03-29', '2019-04-17');
		INSERT INTO cierre VALUES(2019, 5,  3, '2019-03-30', '2019-04-26', '2019-05-15');
		INSERT INTO cierre VALUES(2019, 6,  3, '2019-04-27', '2019-05-24', '2019-06-12');
		INSERT INTO cierre VALUES(2019, 7,  3, '2019-05-25', '2019-06-21', '2019-07-10');
		INSERT INTO cierre VALUES(2019, 8,  3, '2019-06-22', '2019-07-19', '2019-08-07');
		INSERT INTO cierre VALUES(2019, 9,  3, '2019-07-20', '2019-08-16', '2019-09-04');
		INSERT INTO cierre VALUES(2019, 10, 3, '2019-08-17', '2019-09-13', '2019-10-02');
		INSERT INTO cierre VALUES(2019, 11, 3, '2019-09-14', '2019-10-11', '2019-10-30');
		INSERT INTO cierre VALUES(2019, 12, 3, '2019-10-12', '2020-11-08', '2020-11-27');

		INSERT INTO cierre VALUES(2019, 1,  4, '2018-12-09', '2019-01-05', '2019-01-24');
		INSERT INTO cierre VALUES(2019, 2,  4, '2019-01-06', '2019-02-02', '2019-02-21');
		INSERT INTO cierre VALUES(2019, 3,  4, '2019-02-03', '2019-03-02', '2019-03-21');
		INSERT INTO cierre VALUES(2019, 4,  4, '2019-03-03', '2019-03-30', '2019-04-18');
		INSERT INTO cierre VALUES(2019, 5,  4, '2019-03-31', '2019-04-27', '2019-05-16');
		INSERT INTO cierre VALUES(2019, 6,  4, '2019-04-28', '2019-05-25', '2019-06-13');
		INSERT INTO cierre VALUES(2019, 7,  4, '2019-05-26', '2019-06-22', '2019-07-11');
		INSERT INTO cierre VALUES(2019, 8,  4, '2019-06-23', '2019-07-20', '2019-08-08');
		INSERT INTO cierre VALUES(2019, 9,  4, '2019-07-21', '2019-08-17', '2019-09-05');
		INSERT INTO cierre VALUES(2019, 10, 4, '2019-08-18', '2019-09-14', '2019-10-03');
		INSERT INTO cierre VALUES(2019, 11, 4, '2019-09-15', '2019-10-12', '2019-10-31');
		INSERT INTO cierre VALUES(2019, 12, 4, '2019-10-13', '2020-11-09', '2020-11-28');

		INSERT INTO cierre VALUES(2019, 1,  5, '2018-12-10', '2019-01-06', '2019-01-25');
		INSERT INTO cierre VALUES(2019, 2,  5, '2019-01-07', '2019-02-03', '2019-02-22');
		INSERT INTO cierre VALUES(2019, 3,  5, '2019-02-04', '2019-03-03', '2019-03-22');
		INSERT INTO cierre VALUES(2019, 4,  5, '2019-03-04', '2019-03-31', '2019-04-19');
		INSERT INTO cierre VALUES(2019, 5,  5, '2019-04-01', '2019-04-28', '2019-05-17');
		INSERT INTO cierre VALUES(2019, 6,  5, '2019-04-29', '2019-05-26', '2019-06-14');
		INSERT INTO cierre VALUES(2019, 7,  5, '2019-05-27', '2019-06-23', '2019-07-12');
		INSERT INTO cierre VALUES(2019, 8,  5, '2019-06-24', '2019-07-21', '2019-08-07');
		INSERT INTO cierre VALUES(2019, 9,  5, '2019-07-22', '2019-08-18', '2019-09-06');
		INSERT INTO cierre VALUES(2019, 10, 5, '2019-08-19', '2019-09-15', '2019-10-04');
		INSERT INTO cierre VALUES(2019, 11, 5, '2019-09-16', '2019-10-13', '2019-11-01');
		INSERT INTO cierre VALUES(2019, 12, 5, '2019-10-14', '2020-11-10', '2020-11-29');

		INSERT INTO cierre VALUES(2019, 1,  6, '2018-12-11', '2019-01-07', '2019-01-26');
		INSERT INTO cierre VALUES(2019, 2,  6, '2019-01-08', '2019-02-04', '2019-02-23');
		INSERT INTO cierre VALUES(2019, 3,  6, '2019-02-05', '2019-03-04', '2019-03-23');
		INSERT INTO cierre VALUES(2019, 4,  6, '2019-03-05', '2019-04-01', '2019-04-20');
		INSERT INTO cierre VALUES(2019, 5,  6, '2019-04-02', '2019-04-29', '2019-05-18');
		INSERT INTO cierre VALUES(2019, 6,  6, '2019-04-30', '2019-05-27', '2019-06-15');
		INSERT INTO cierre VALUES(2019, 7,  6, '2019-05-28', '2019-06-24', '2019-07-13');
		INSERT INTO cierre VALUES(2019, 8,  6, '2019-06-25', '2019-07-22', '2019-08-08');
		INSERT INTO cierre VALUES(2019, 9,  6, '2019-07-23', '2019-08-19', '2019-09-07');
		INSERT INTO cierre VALUES(2019, 10, 6, '2019-08-20', '2019-09-16', '2019-10-05');
		INSERT INTO cierre VALUES(2019, 11, 6, '2019-09-17', '2019-10-14', '2019-11-02');
		INSERT INTO cierre VALUES(2019, 12, 6, '2019-10-15', '2020-11-11', '2020-11-30');

		INSERT INTO cierre VALUES(2019, 1,  7, '2018-12-12', '2019-01-08', '2019-01-27');
		INSERT INTO cierre VALUES(2019, 2,  7, '2019-01-09', '2019-02-05', '2019-02-24');
		INSERT INTO cierre VALUES(2019, 3,  7, '2019-02-06', '2019-03-05', '2019-03-24');
		INSERT INTO cierre VALUES(2019, 4,  7, '2019-03-06', '2019-04-02', '2019-04-21');
		INSERT INTO cierre VALUES(2019, 5,  7, '2019-04-03', '2019-04-30', '2019-05-19');
		INSERT INTO cierre VALUES(2019, 6,  7, '2019-05-01', '2019-05-28', '2019-06-16');
		INSERT INTO cierre VALUES(2019, 7,  7, '2019-05-29', '2019-06-25', '2019-07-14');
		INSERT INTO cierre VALUES(2019, 8,  7, '2019-06-26', '2019-07-23', '2019-08-09');
		INSERT INTO cierre VALUES(2019, 9,  7, '2019-07-24', '2019-08-20', '2019-09-08');
		INSERT INTO cierre VALUES(2019, 10, 7, '2019-08-21', '2019-09-17', '2019-10-06');
		INSERT INTO cierre VALUES(2019, 11, 7, '2019-09-18', '2019-10-15', '2019-11-03');
		INSERT INTO cierre VALUES(2019, 12, 7, '2019-10-16', '2020-11-12', '2020-12-01');

		INSERT INTO cierre VALUES(2019, 1,  8, '2018-12-13', '2019-01-09', '2019-01-28');
		INSERT INTO cierre VALUES(2019, 2,  8, '2019-01-10', '2019-02-06', '2019-02-25');
		INSERT INTO cierre VALUES(2019, 3,  8, '2019-02-07', '2019-03-06', '2019-03-25');
		INSERT INTO cierre VALUES(2019, 4,  8, '2019-03-07', '2019-04-03', '2019-04-22');
		INSERT INTO cierre VALUES(2019, 5,  8, '2019-04-04', '2019-05-01', '2019-05-20');
		INSERT INTO cierre VALUES(2019, 6,  8, '2019-05-02', '2019-05-29', '2019-06-17');
		INSERT INTO cierre VALUES(2019, 7,  8, '2019-05-30', '2019-06-26', '2019-07-15');
		INSERT INTO cierre VALUES(2019, 8,  8, '2019-06-27', '2019-07-24', '2019-08-10');
		INSERT INTO cierre VALUES(2019, 9,  8, '2019-07-25', '2019-08-21', '2019-09-09');
		INSERT INTO cierre VALUES(2019, 10, 8, '2019-08-22', '2019-09-18', '2019-10-07');
		INSERT INTO cierre VALUES(2019, 11, 8, '2019-09-19', '2019-10-16', '2019-11-04');
		INSERT INTO cierre VALUES(2019, 12, 8, '2019-10-17', '2020-11-13', '2020-12-02');

		INSERT INTO cierre VALUES(2019, 1,  9, '2018-12-14', '2019-01-10', '2019-01-29');
		INSERT INTO cierre VALUES(2019, 2,  9, '2019-01-11', '2019-02-07', '2019-02-26');
		INSERT INTO cierre VALUES(2019, 3,  9, '2019-02-08', '2019-03-07', '2019-03-26');
		INSERT INTO cierre VALUES(2019, 4,  9, '2019-03-08', '2019-04-04', '2019-04-23');
		INSERT INTO cierre VALUES(2019, 5,  9, '2019-04-05', '2019-05-02', '2019-05-21');
		INSERT INTO cierre VALUES(2019, 6,  9, '2019-05-03', '2019-05-30', '2019-06-18');
		INSERT INTO cierre VALUES(2019, 7,  9, '2019-05-31', '2019-06-27', '2019-07-16');
		INSERT INTO cierre VALUES(2019, 8,  9, '2019-06-28', '2019-07-25', '2019-08-11');
		INSERT INTO cierre VALUES(2019, 9,  9, '2019-07-26', '2019-08-22', '2019-09-10');
		INSERT INTO cierre VALUES(2019, 10, 9, '2019-08-23', '2019-09-19', '2019-10-08');
		INSERT INTO cierre VALUES(2019, 11, 9, '2019-09-20', '2019-10-17', '2019-11-05');
		INSERT INTO cierre VALUES(2019, 12, 9, '2019-10-18', '2020-11-14', '2020-12-03');

		
		--INSERT INTO compra VALUES (0, 3489558385502970, 1, '2019-05-21 21:09:08.332681', 10.00, false);
		--INSERT INTO compra VALUES (1, 3489558385502970, 2, '2019-05-21 19:39:15.444444', 50.00, false);                                                         
		--INSERT INTO compra VALUES (2, 3748427341747440, 19, '2019-04-19 13:40:10.369852', 10.00, false);
		--INSERT INTO compra VALUES (3, 3748427341747440, 18, '2019-04-19 13:51:15.123654', 10.00, false);
		--INSERT INTO compra VALUES (4, 3489558385502970, 15, '2019-10-17 13:53:40.456123', 79.00, false);
		--INSERT INTO compra VALUES (5, 3489558385502970, 17, '2019-05-01 20:24:37.523698', 103.00, false);
		--INSERT INTO compra VALUES (6, 3489558385502970, 10, '2019-05-07 15:17:50.478965', 35.00, false);
		--INSERT INTO compra VALUES (7, 3489558385502970, 9, '2019-07-16 17:49:42.456321', 489.00, false);
		--INSERT INTO compra VALUES (8, 3489558385502970, 7, '2019-11-27 15:16:15.142536', 74.00, false);
		--INSERT INTO compra VALUES (9, 3489558385502970, 5, '2019-04-25 19:35:03.197364', 100.00, false);
		--INSERT INTO compra VALUES (10, 3897588866746057, 8, '2019-04-25 06:52:07.823791', 100.00, false);
		--INSERT INTO compra VALUES (11, 3897588866746057, 3, '2019-04-19 10:18:02.591735', 153.00, false);
		--INSERT INTO compra VALUES (12, 4664383242476058, 16, '2019-04-17 09:39:08.486213', 2569.00, false);
	`)

	if err != nil {
		log.Fatal(err)
	}
}


func insertConsumo() {
	db := sqldb

	//Generador de Resumen
	_, err := db.Exec(`
				
				--RECHAZO compra tarjeta invalida 
				INSERT INTO consumo VALUES ('34895583855aaaaa','5060',1,1000.00);

				--RECHAZO compra tarjeta no vigente
				INSERT INTO consumo VALUES ('4559588866746051','8100',10,1000.00);
				
				--RECHAZO compra tarjeta suspendida
				INSERT INTO consumo VALUES ('5457557367903750','4280',2,2500.00);
				
				--RECHAZO codigo de seguridad incorrecto
				INSERT INTO consumo VALUES ('5442334993275369','8711',3,500.00);
				
				--RECHAZO exceso limite de compra
				INSERT INTO consumo VALUES ('3489558385502970','5060',3,6000.00);
				
				--INSERTAR COMPRA
				INSERT INTO consumo VALUES ('3489558385502970','5060',3,2500.00);
				
				--INSERTAR COMPRA -- GENERA ALERTA
				INSERT INTO consumo VALUES ('3489558385502970','5060',15,50.00);

				--INSERTAR COMPRA -- GENERA ALERTA
				INSERT INTO consumo VALUES ('3489558385502970','5060',19,50.00);

				--RECHAZO exceso limite de compra GENERA ALERTA DOBLE RECHAZO
				INSERT INTO consumo VALUES ('3489558385502970','5060',3,3500.00);

				--RECHAZO por plazo de vigencia expirado
				INSERT INTO consumo VALUES ('5102620553138109','3550',3,3500.00);
				
				--INSERTAR COMPRAS VALIDAS
				INSERT INTO consumo VALUES ('343915672231656','5570',3,1500.00);
				INSERT INTO consumo VALUES ('343915672231656','5570',3,1000.20);
				INSERT INTO consumo VALUES ('343915672231656','5570',3,1002.20);
				INSERT INTO consumo VALUES ('343915672231656','5570',3,400.20);
				INSERT INTO consumo VALUES ('4619521540625191','9870',3,200.20);
				INSERT INTO consumo VALUES ('4619521540625191','9870',3,100.30);
				INSERT INTO consumo VALUES ('4619521540625191','9870',3, 10.00);
				

	`)

	if err != nil {
		log.Fatal(err)
	}
}


func testSystem() {
	db := sqldb
	_, err := db.Exec(`
		CREATE OR REPLACE FUNCTION testsystem() RETURNS VOID AS $$
		DECLARE
		consumoAux record;
		BEGIN
			FOR consumoAux IN SELECT * FROM consumo LOOP
				PERFORM insertarcompra(consumoAux.nrotarjeta,consumoAux.codseguridad,consumoAux.nrocomercio,consumoAux.monto);	
			END LOOP;
		END;
		$$ language plpgsql;

		select testsystem();

		select generarresumen(5,2019,6);
		select generarresumen(5,2019,7);
		select generarresumen(5,2019,8);
	`)

	if err != nil {
		log.Fatal(err)
	}
}


func generateSummary() {
	db := sqldb

	//Generador de Resumen
	_, err := db.Exec(`
		CREATE OR REPLACE FUNCTION generarResumen(clienteNro int, añoBuscado int, mesBuscado int) RETURNS VOID AS $$
			DECLARE
				tarjetasAux record;
				clienteAux record;
				compras record;
		
				desde date;
				hasta date;
				vence date;

				nroResumen int;
				lineaAux int;
				comercioAux text;

				resumenTotal decimal(8,2);

			BEGIN
				SELECT * INTO clienteAux FROM cliente WHERE nrocliente = clienteNro;

				FOR tarjetasAux IN SELECT * FROM tarjeta WHERE nrocliente = clienteNro LOOP

					nroResumen := (SELECT CONCAT(SUBSTR(tarjetasAux.nrotarjeta, 1, 3), añoBuscado, mesBuscado)::int);

					SELECT fechainicio INTO desde FROM cierre 
					WHERE cierre.terminacion = right(tarjetasAux.nrotarjeta, 1)::int and cierre.mes = mesBuscado and cierre.año = añoBuscado;

					SELECT fechacierre INTO hasta FROM cierre 
					WHERE cierre.terminacion = right(tarjetasAux.nrotarjeta, 1)::int and cierre.mes = mesBuscado and cierre.año = añoBuscado;

					SELECT fechavto INTO vence FROM cierre 
					WHERE cierre.terminacion = right(tarjetasAux.nrotarjeta, 1)::int and cierre.mes = mesBuscado and cierre.año = añoBuscado;

					SELECT SUM(monto) INTO resumenTotal from compra
					WHERE compra.nrotarjeta = tarjetasAux.nrotarjeta and pagado='false' and fecha::date between desde and hasta;   

					IF resumenTotal is null THEN
						resumenTotal = 0;
					END IF;

					INSERT INTO cabecera VALUES(nroResumen, clienteAux.nombre, clienteAux.apellido, clienteAux.domicilio, 
						tarjetasAux.nrotarjeta, desde, hasta, vence, resumenTotal);

					/*--------------------------------------------------------------------------------------------------------*/
					FOR compras IN SELECT * FROM compra WHERE tarjetasAux.nrotarjeta = compra.nrotarjeta LOOP

					IF compras.fecha::date between desde and hasta THEN
						SELECT (MAX(detalle.nrolinea) + 1) into lineaAux from detalle;

						SELECT comercio.nombre INTO comercioAux FROM comercio
						WHERE compras.nrocomercio = comercio.nrocomercio;

						IF lineaAux is null THEN
							lineaAux = 0;
						END IF;

					INSERT INTO detalle VALUES(nroResumen, lineaAux, compras.fecha, comercioAux, compras.monto); 
					END IF;

					END LOOP;
					UPDATE compra set pagado = true WHERE tarjetasAux.nrotarjeta = compra.nrotarjeta
						and pagado='false' and fecha::date between desde and hasta;
				END LOOP;
			END;
		$$ language plpgsql;
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func executePurchase() {
	db := sqldb

	//Autorizar compra
	_, err := db.Exec(`
		--RETORNA VERDADERO SI LA TARJETA ES VALIDA
		create or replace function validar_tarjeta_nro(p_nrotarjeta char(16)) returns boolean as $$
		declare
			estado boolean=false;
		begin
		if exists (select nrotarjeta from tarjeta where nrotarjeta=p_nrotarjeta) then
			return true;
		end if;
			return false;
		end;
		$$ language plpgsql;

		--RETORNA VERDADERO SI LA TARJETA SE ENCUENTRA VIGENTE
		create or replace function validar_tarjeta_vigente(p_nrotarjeta char(16)) returns boolean as $$
		declare
			var int default 0;
		begin
			select count(1) into var from tarjeta where estado='vigente' and nrotarjeta=p_nrotarjeta;
		IF var > 0 THEN
			return true;
		end if;
			return false;
		end;
		$$ language plpgsql;


		--RETORNA VERDADERO SI EL CODIGO DE SEGURIDAD CORRESPONDE A LA TARJETA
		create or replace function validar_tarjeta_codseg(p_nrotarjeta char(16), p_codseg char(4)) returns boolean as $$
		declare
			var int default 0;
		begin
			select 1 into var from tarjeta where codseguridad=p_codseg and nrotarjeta=p_nrotarjeta;
		IF var > 0 THEN
			return true;
		end if;
			return false;
		end;
		$$ language plpgsql;

		--RETORNA VERDADERO SI LA TARJETA NO SE ENCUENTRA SUSPENDIDA

		create or replace function validar_tarjeta_suspendida(p_nrotarjeta char(16)) returns boolean as $$
		declare
			var int default 0;
		begin
			select count(1) into var from tarjeta where estado='suspendida' and nrotarjeta=p_nrotarjeta;
		IF var > 0 THEN
			return false;
		end if;
			return true;
		end;
		$$ language plpgsql;

		--RETORNA VERDADERO SI LA TARJETA SE ENCUENTRA ACTIVA
		create or replace function validar_tarjeta_vencida(p_nrotarjeta char(16), fecha date) returns boolean as $$
		declare
			var int default 0;
			resultado record;
			var_desde char(12);
			var_fecha_desde date;
			var_hasta char(12);
			var_fecha_hasta date;
		begin
			select * into resultado from tarjeta where nrotarjeta=p_nrotarjeta;
			if not found then
					raise notice 'la tarjeta % es inválida', p_nrotarjeta;
			else
				var_desde := (substring(resultado.validadesde from 0 for 5))||(substring(resultado.validadesde from 5 for 2))||'01'; 
				var_fecha_desde :=to_date(var_desde,'YYYYMMDD');
				var_hasta := (substring(resultado.validahasta from 0 for 5))|| '-'||(substring(resultado.validahasta from 5 for 2))||'-01'; 
				var_fecha_hasta := (date_trunc('month', var_hasta ::date) + interval '1 month' - interval '1 day')::date;
				select count(1) into var from tarjeta where nrotarjeta=p_nrotarjeta and fecha between var_fecha_desde and var_fecha_hasta;
				IF var > 0 THEN
					return true;
				else
					return false;
				end if;
			end if;
			return true;
		end;
		$$ language plpgsql;

		--RETORNA VERDADERO SI SE PUEDE REALIZAR LA COMPRA

		create or replace function validar_tarjeta_compras_pendientes(p_nrotarjeta char(16), monto_compra_actual decimal(8,2)) returns boolean as $$
		declare
			varLimite decimal(8,2) default 0;
			varMontoGastado decimal(8,2) default 0;
			resultado record;
			comprarecord record;
		begin
			select limitecompra into varLimite from tarjeta where nrotarjeta=p_nrotarjeta;
			select * into comprarecord from compra where nrotarjeta=p_nrotarjeta and pagado='false';
			if not found then
				varMontoGastado := 0;
			else
				select SUM(monto) into varMontoGastado from compra where nrotarjeta=p_nrotarjeta and pagado='false';
			end if;
			if((varMontoGastado+monto_compra_actual)<= varLimite)then
				return true;
			else
				return false;
			end if;
		end;
		$$ language plpgsql;


		create or replace function insertar_rechazo(p_nrotarjeta char(16), p_nrocomercio int, p_fecha timestamp, p_monto decimal(7,2), p_motivo text) returns void as $$
		declare
				var int;
				rechazado record;
		begin
				select * into rechazado from rechazo where nrorechazo=1;
				if not found then
					var := 1;
				else
					select max(nrorechazo) into var from rechazo;
					var := var+1;
				end if;
					insert into rechazo values (var, p_nrotarjeta, p_nrocomercio,p_fecha, p_monto,p_motivo);
		end;
		$$ language plpgsql;


		create or replace function agregar_compra(p_nrotarjeta char(16),p_nrocomercio int,p_fecha timestamp,p_monto decimal(7,2),p_pagado boolean) returns void as $$
		declare
				var int;
				comprar record;
		begin
				select * into comprar from compra where nrooperacion=1;
				if not found then
					var := 1;
				else
					select max(nrooperacion) into var from compra;
					var := var+1;
				end if;
					insert into compra values(var,p_nrotarjeta,p_nrocomercio,p_fecha,p_monto,p_pagado);
		end;
		$$ language plpgsql;


		create or replace function insertarcompra(p_nrotarjeta char(16), codseguridad char(4),nrocomercio int, monto decimal(7,2)) returns boolean as $$
		declare
		estado boolean;
		motivo text;
		begin
			estado := true;
			if(select validar_tarjeta_nro(p_nrotarjeta)) then
		raise notice 'la tarjeta es válida';

		if(select validar_tarjeta_vigente(p_nrotarjeta)) THEN
			raise notice 'la tarjeta es vigente';

			if(select validar_tarjeta_codseg(p_nrotarjeta,codseguridad)) THEN
				raise notice 'El codigo de seguridad es correcto';

				if(select validar_tarjeta_compras_pendientes(p_nrotarjeta,monto)) THEN
					raise notice 'No supera el limite de la tarjeta';

					if(select validar_tarjeta_vencida(p_nrotarjeta,CURRENT_DATE)) THEN
						raise notice 'La tarjeta no se encuentra vencida';

				ELSE
					raise notice 'Plazo de vigencia expirado';
					estado := false;
					motivo := 'Plazo de vigencia expirado';
				end if;

				ELSE
					raise notice 'Supera el lìmite de compra';
					estado := false;
					motivo := 'supera límite de tarjeta';
				end if;

			ELSE
				raise notice 'El codigo de seguridad no es correcto';
				estado := false;
				motivo := 'El codigo de seguridad no es correcto';
			end if;

		ELSE
			raise notice 'la tarjeta no es vigente';
			estado := false;
			motivo := 'La tarjeta no es vigente';

			--AGREGO CODIGO DE TARJETA SUSPENDIDA

			if(select validar_tarjeta_suspendida(p_nrotarjeta)) THEN
				raise notice 'La tarjeta no se encuentra suspendida';
				ELSE
				raise notice 'La tarjeta se encuentra suspendida';
				PERFORM insertar_rechazo(p_nrotarjeta,nrocomercio::int, CURRENT_TIMESTAMP::timestamp,monto, 'La tarjeta se encuentra suspendida');
			end if;
		end if;

			else
				raise notice 'la tarjeta es inválida se guarda rechazo';
				motivo := 'La tarjeta es invalida';
				estado := false;
				end if;

			if(estado = false)then
				PERFORM insertar_rechazo(p_nrotarjeta,nrocomercio::int, CURRENT_TIMESTAMP::timestamp,monto, motivo);
			else
				PERFORM agregar_compra(p_nrotarjeta,nrocomercio,CURRENT_TIMESTAMP::timestamp,monto,false);
			end if;

				return true;
			end;
			$$ language plpgsql;
	`)

	if err != nil {
		log.Fatal(err)
	}
}

func createTriggerFunctions() {
	createRejectionAlert()
	createPurchaseCheckers()
	createDoubleExcessLimit()
}

func createRejectionAlert() {
	statement := `
		CREATE SEQUENCE IF NOT EXISTS alerta_id_seq;

		CREATE OR REPLACE FUNCTION insert_alert() RETURNS TRIGGER AS $$
			DECLARE
				codalerta INT;
			BEGIN
				codalerta = 0;

				IF NEW.motivo = 'supera límite de tarjeta' THEN
					codalerta = 32;
				END IF;

				INSERT INTO alerta VALUES (
					nextval('alerta_id_seq'::regclass),
					NEW.nrotarjeta,
					NEW.fecha,
					NEW.nrorechazo,
					codalerta,
					NEW.motivo
				);

				RETURN NULL;
			END;
		$$ LANGUAGE plpgsql;

		DROP TRIGGER IF EXISTS alerts_trigger ON rechazo;

		CREATE TRIGGER alerts_trigger AFTER INSERT ON rechazo
		FOR EACH ROW EXECUTE PROCEDURE insert_alert();`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func createPurchaseCheckers() {
	statement := `
		CREATE OR REPLACE FUNCTION check_purchases_one_min() RETURNS TRIGGER AS $$
			BEGIN
				PERFORM
				FROM
					compra c
					INNER JOIN comercio co ON co.nrocomercio = c.nrocomercio
				WHERE
					c.nrotarjeta = NEW.nrotarjeta
					AND c.nrocomercio != NEW.nrocomercio
					AND co.codigopostal = (
						SELECT codigopostal
						FROM comercio
						WHERE nrocomercio = NEW.nrocomercio
					)
					AND (NEW.fecha - c.fecha) < INTERVAL '1 min';

				IF FOUND THEN
					INSERT INTO alerta VALUES (
						nextval('alerta_id_seq'::regclass),
						NEW.nrotarjeta,
						NEW.fecha,
						NULL,
						1,
						'compra 1min'
					);
				END IF;

				RETURN NEW;
			END;
		$$ LANGUAGE plpgsql;

		CREATE OR REPLACE FUNCTION check_purchases_five_min() RETURNS TRIGGER AS $$
			BEGIN
				PERFORM
				FROM
					compra c
					INNER JOIN comercio co ON co.nrocomercio = c.nrocomercio
				WHERE
					c.nrotarjeta = NEW.nrotarjeta
					AND co.codigopostal != (
						SELECT codigopostal
						FROM comercio
						WHERE nrocomercio = NEW.nrocomercio
					)
					AND (NEW.fecha - c.fecha) < INTERVAL '5 min';

				IF FOUND THEN
					INSERT INTO alerta VALUES (
						nextval('alerta_id_seq'::regclass),
						NEW.nrotarjeta,
						NEW.fecha,
						NULL,
						5,
						'compra 5min'
					);
				END IF;

				RETURN NEW;
			END;
		$$ LANGUAGE plpgsql;

		DROP TRIGGER IF EXISTS one_min_purchase_trigger ON compra;

		DROP TRIGGER IF EXISTS five_min_purchase_trigger ON compra;

		CREATE TRIGGER one_min_purchase_trigger BEFORE INSERT ON compra
		FOR EACH ROW EXECUTE PROCEDURE check_purchases_one_min();

		CREATE TRIGGER five_min_purchase_trigger BEFORE INSERT ON compra
		FOR EACH ROW EXECUTE PROCEDURE check_purchases_five_min();`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

func createDoubleExcessLimit() {
	statement := `
		CREATE OR REPLACE FUNCTION double_excess_limit_alert() RETURNS TRIGGER AS $$
			DECLARE
				rejections INT;
				reason CHAR(30);
			BEGIN
				reason = 'supera límite de tarjeta';
				
				IF NEW.motivo = reason THEN
					SELECT count(*)
					INTO rejections
					FROM rechazo r
					WHERE
						r.motivo = reason
						AND r.nrotarjeta = NEW.nrotarjeta
						AND r.fecha::DATE = NEW.fecha::DATE;

					IF rejections > 1 THEN
						UPDATE tarjeta SET estado = 'suspendida' WHERE nrotarjeta = NEW.nrotarjeta;

						INSERT INTO alerta VALUES (
							nextval('alerta_id_seq'::regclass),
							NEW.nrotarjeta,
							NEW.fecha,
							NEW.nrorechazo,
							0,
							'doble rechazo por límite'
						);
					END IF;
				END IF;

				RETURN NEW;
			END;
		$$ LANGUAGE plpgsql;

		DROP TRIGGER IF EXISTS double_excess_limit_trigger ON rechazo;

		CREATE TRIGGER double_excess_limit_trigger AFTER INSERT ON rechazo
		FOR EACH ROW EXECUTE PROCEDURE double_excess_limit_alert();`

	if _, err := sqldb.Exec(statement); err != nil {
		log.Fatal(err)
	}
}

//endregion

// #region NoSQL Operations
func createInstances() {
	customers = []Cliente{
		Cliente{
			NroCliente: 1,
			Nombre:     "Motoko",
			Apellido:   "Kusanagi",
			Domicilio:  "Sección de Seguridad Pública 9",
			Telefono:   "81345720198",
		},
		Cliente{
			NroCliente: 2,
			Nombre:     "Rick",
			Apellido:   "Deckard",
			Domicilio:  "100 West 1st Street",
			Telefono:   "18772755819",
		},
		Cliente{
			NroCliente: 3,
			Nombre:     "Thomas",
			Apellido:   "Anderson",
			Domicilio:  "Tucumán 1938",
			Telefono:   "54115352760",
		},
	}

	cards = []Tarjeta{
		Tarjeta{
			NroTarjeta:   "4388883134685315",
			NroCliente:   1,
			ValidaDesde:  "201801",
			ValidaHasta:  "202801",
			CodSeguridad: "165",
			LimiteCompra: 50000.00,
			Estado:       "vigente",
		},
		Tarjeta{
			NroTarjeta:   "4292047837584853",
			NroCliente:   2,
			ValidaDesde:  "202204",
			ValidaHasta:  "202704",
			CodSeguridad: "948",
			LimiteCompra: 25000.00,
			Estado:       "suspendida",
		},
		Tarjeta{
			NroTarjeta:   "4427009978209284",
			NroCliente:   3,
			ValidaDesde:  "202007",
			ValidaHasta:  "202607",
			CodSeguridad: "746",
			LimiteCompra: 30000.00,
			Estado:       "anulada",
		},
	}

	stores = []Comercio{
		Comercio{
			NroComercio:  1,
			Nombre:       "Pizzería Los Hijos de Put4",
			Domicilio:    "Donovan y Camino Gral. Chamizo",
			CodigoPostal: "1824",
			Telefono:     "No hay teléfono",
		},
		Comercio{
			NroComercio:  2,
			Nombre:       "Heladería Los Dos Gustos",
			Domicilio:    "Camino Gral. Chamizo y Donovan",
			CodigoPostal: "1824",
			Telefono:     "62473489",
		},
		Comercio{
			NroComercio:  3,
			Nombre:       "La Angioplastía",
			Domicilio:    "Sarrasagastti 2431, Sarandí",
			CodigoPostal: "1872",
			Telefono:     "1122334455",
		},
	}

	purchases = []Compra{
		Compra{
			NroOperacion: 2,
			NroTarjeta:   "4388883134685315",
			NroComercio:  1,
			Fecha:        "2019-01-01",
			Monto:        112358.13,
			Pagado:       true,
		},
		Compra{
			NroOperacion: 4,
			NroTarjeta:   "4292047837584853",
			NroComercio:  2,
			Fecha:        "2019-06-01",
			Monto:        3.1415926,
			Pagado:       true,
		},
		Compra{
			NroOperacion: 6,
			NroTarjeta:   "4427009978209284",
			NroComercio:  3,
			Fecha:        "2019-12-01",
			Monto:        2.7182818,
			Pagado:       true,
		},
	}
}

func writeCustomers() {
	for _, customer := range customers {
		data, err := json.Marshal(customer)
		if err != nil {
			log.Fatal(err)
		}

		write("clientes", []byte(strconv.Itoa(customer.NroCliente)), data)
	}
}

func writeCards() {
	for _, card := range cards {
		data, err := json.Marshal(card)
		if err != nil {
			log.Fatal(err)
		}

		write("tarjetas", []byte(card.NroTarjeta), data)
	}
}

func writeStores() {
	for _, store := range stores {
		data, err := json.Marshal(store)
		if err != nil {
			log.Fatal(err)
		}

		write("comercios", []byte(strconv.Itoa(store.NroComercio)), data)
	}
}

func writePurchases() {
	for _, purchase := range purchases {
		data, err := json.Marshal(purchase)
		if err != nil {
			log.Fatal(err)
		}

		write("compras", []byte(strconv.Itoa(purchase.NroOperacion)), data)
	}
}

func write(name string, key []byte, data []byte) {
	tx, err := boltdb.Begin(true)
	if err != nil {
		log.Fatal(err)
	}

	defer tx.Rollback()

	bucket, err := tx.CreateBucketIfNotExists([]byte(name))
	if err != nil {
		log.Fatal(err)
	}

	if err := bucket.Put(key, data); err != nil {
		log.Fatal(err)
	}

	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
}

func read(name string, key []byte) {
	var buffer []byte

	if err := boltdb.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(name))
		buffer = b.Get(key)
		return nil
	}); err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(buffer))
}

//endregion
