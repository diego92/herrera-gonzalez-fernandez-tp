package promptutil

import (
	"errors"
	"fmt"
	"log"
	"strconv"
)

// Menu muestra un menú y espera que se seleccione una opción,
// si hay error lo maneja, sino devuelve la opción seleccionada.
// items es la lista de opciones a mostrar.
func Menu(items []string) int {
	choice, err := prompt(items)

	if err != nil {
		log.Fatal(err)
	}

	return choice
}

// prompt muestra una lista ordenada y pide que se seleccione una opción.
// items lista de opciones que se quieran mostrar.
func prompt(items []string) (int, error) {
	// muestro la lista
	printOrderedSlice(&items)

	var choice int

	// obtengo la opción del usuario
	if _, err := fmt.Scanln(&choice); err != nil {
		return choice, err
	}

	// verifico que sea una opción válida
	if choice < 1 || choice > len(items) {
		return choice, errors.New("The selected number (" + strconv.Itoa(choice) + ") is not a valid option.")
	}

	return choice, nil
}

// printOrderedSlice imprime en pantalla un slice de string de manera ordenada.
// slice es el slice de string que se quiere mostrar.
func printOrderedSlice(slice *[]string) {
	for k, v := range *slice {
		fmt.Printf("%d. %s\n", k+1, v)
	}
}
